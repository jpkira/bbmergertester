package com.abcbcode.bbmergertester.linedata

import java.io.Serializable
import java.sql.Blob
import javax.persistence.*

@Entity
@Table(name = "XXARC_ODS_BILLING_BACKUP_LINE_DATA_TBL")
data class BillingBackupLineData(

        @Id
        @Column(name = "SEQ_NUM")
        val seqNum: Long = 0,

        @Column(name = "INVOICE_ID")
        val invoiceId: Long = 0,

        @Column(name = "INVOICE_LINE_ID")
        val invoiceLineId: Long = 0,

        @Column(name = "OIC_FLOWID")
        val oicFlowId: String = "",

        @Column(name = "JOB_ID")
        val jobId: String? = null,

        @Column(name = "ORG_JOB_ID")
        val originalJobId: String? = null,

        @Column(name = "INVOICE_LINE_NUMBER")
        val invoiceLineNumber: Long? = null,

        @Column(name = "INVOICE_TRAN_NUMBER")
        val invoiceTranNumber: String? = null,

        @Column(name = "DOC_ID")
        val docId: Long? = null,

        @Column(name = "PROJECT_NUMBER")
        val projectNumber: String = "",

        @Column(name = "TASK_NUMBER")
        val taskNumber: String? = "",

        @Column(name = "DOC_SEQ")
        val docSeq: Long? = null,

        @Column(name = "DOC_COVERSHEET")
        val docCoverSheet: String? = null,

        @Column(name = "DOC_TYPE")
        val docType: String? = null,

        @Column(name = "EXP_TYPE")
        val expenseType: String? = null,

        @Column(name = "MIME_TYPE")
        val mimeType: String = "",

        @Lob
        @Column(name = "BIP_DOC")
        val bipDoc: String? = null,

        @Lob
        @Column(name = "BIP_DOC_CONVERTED")
        val bipDocConverted: Blob? = null

) : Serializable
