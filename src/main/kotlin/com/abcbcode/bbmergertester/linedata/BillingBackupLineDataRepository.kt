package com.abcbcode.bbmergertester.linedata


import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

/**
 * BillingBackupLineDataRepository is the repository interface for managing the BillingBackupLineData Entity class
 */

@Repository
interface BillingBackupLineDataRepository : JpaRepository<BillingBackupLineData, Long> {

    @Query(value = "SELECT b FROM BillingBackupLineData b " +
            "WHERE b.originalJobId = ?1 " +
            "ORDER BY b.originalJobId, b.invoiceLineId, b.docType, b.docCoverSheet, b.projectNumber")
    fun findByOriginalJobId(originalJobId : String) : List<BillingBackupLineData>
}
