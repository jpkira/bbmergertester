package com.abcbcode.bbmergertester

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BbmergertesterApplication

fun main(args: Array<String>) {
    runApplication<BbmergertesterApplication>(*args)
}
